module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    babelLoader: {compact: true},
                    loader: "babel-loader"
                },
            }
        ]
    },
}