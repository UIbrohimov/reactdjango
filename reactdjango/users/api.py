from .models import User
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import UserSerializer


# get the default router of rest framework


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    model = User
    queryset = User.objects.all().order_by('id')
    serializer_class = UserSerializer
    permission_classes = [permissions.AllowAny]
