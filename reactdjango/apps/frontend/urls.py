from unicodedata import name
from django.urls import path

from .views import index


app_name = "frontend"
urlpatterns = [
    path('', view=index, name="home")
]
