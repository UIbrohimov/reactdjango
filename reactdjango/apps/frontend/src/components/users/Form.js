import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { addUser } from "../../actions/users";

export class Form extends Component {
  state = {
    username: "",
    email: "",
  };

  static propTypes = {
    addUser: PropTypes.func.isRequired,
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    // const { name, email } = this.state
    // const user = { name, email }
    this.props.addUser(this.state);
  };

  render() {
    const { username, email } = this.state;
    return (
      <div className="card card-body mt-4 mb-4">
        <h2>Add User</h2>
        <form action="#" onSubmit={this.onSubmit}>
          <div className="form-group">
            <label htmlFor="username">Name:</label>
            <input
              type="text"
              name="username"
              id="name"
              onChange={this.onChange}
              value={username}
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              name="email"
              id="email"
              onChange={this.onChange}
              value={email}
              className="form-control"
            />
          </div>
          <div className="form-group">
            <input
              type="submit"
              className="form-control btn btn-primary mt-3"
            />
          </div>
        </form>
      </div>
    );
  }
}

export default connect(null, { addUser })(Form);
