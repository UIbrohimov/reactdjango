import axios from "axios";
import Swal from "sweetalert2";

import { GET_USERS, DELETE_USER, ADD_USER } from "./types";

// get users
export const getUsers = () => (dispatch) => {
  axios
    .get("/api/")
    .then((res) => {
      dispatch({
        type: GET_USERS,
        payload: res.data,
      });
    })
    .catch((err) => console.log(err));
};

// delete a user
export const deleteUser = (id) => (dispatch) => {
  axios
    .delete(`/api/${id}/`)
    .then((res) => {
      dispatch({
        type: DELETE_USER,
        payload: id,
      });
      Swal.fire({
        title: "User has deleted successfully",
        // text: 'Do you want to continue',
        icon: 'success',
        confirmButtonText: 'Good'
      })
    })
    .catch((err) => Swal.fire({
      title: err.response.data.message,
      // text: 'Do you want to continue',
      icon: 'error',
      confirmButtonText: 'Retry'
    })
  );
};

// add user
export const addUser = (user) => (dispatch) => {
  console.log("user", user);
  axios
    .post("/api/", user)
    .then((res) => {
      dispatch({
        type: ADD_USER,
        payload: res.data,
      });
      Swal.fire({
        title: "User is added successfully",
        // text: 'Do you want to continue',
        icon: 'success',
        confirmButtonText: 'Great'
      })
    })
    .catch((err) =>  Swal.fire({
        title: err.response.data.message,
        // text: ,
        icon: 'error',
        confirmButtonText: 'Understand',
        timer: 2000
      })
    );
};
